import functools
import hashlib
import json
# Recompensa a los mineros
MINING_REWARD = 10

# El bloque que tenemos al principio de todo
genesis_block = {
    'previous_hash': '',
    'index': 0,
    'transactions': []
}
# Iniciamos la lista del blockchain vacia
blockchain = [genesis_block]
# Transacciones no manejadas
open_transactions = []
# De momento me dejo como dueno de este nodo
owner = 'Flavius'
# Registro participantes
participants = {'Flavius'}


def hash_block(block):
    """Hashes el bloque y devuelve un string con el hash.

    Arguments:
        :block: El bloque que se va a hashear.
    """
    return hashlib.sha256(json.dumps(block).encode()).hexdigest()


def get_balance(participant):
    
    """Calcula y retorna el balance de un participante.

    Arguments:
        :participant: La persona de la que calculamos el balance total.
    """
    # Obtiene de la cantidad de monedas enviadas por una persona en concreto.
    # Recupera cantidades de transacciones que ya estaban incluidas en bloques del "blockchain".
    tx_sender = [[tx['amount'] for tx in block['transactions']
                  if tx['sender'] == participant] for block in blockchain]
    # Obtiene la una lista de las monedas que hay en una transaccion abierta.
    # Lista de las transacciones abiertas
    open_tx_sender = [tx['amount']
                      for tx in open_transactions if tx['sender'] == participant]
    tx_sender.append(open_tx_sender)
    amount_sent = reduce(lambda tx_sum, tx_amt: tx_sum + sum(tx_amt)
                         if len(tx_amt) > 0 else tx_sum + 0, tx_sender, 0)
    # Lista de las monedas que hay en el bloque
    tx_recipient = [[tx['amount'] for tx in block['transactions']
                     if tx['recipient'] == participant] for block in blockchain]
    amount_received = reduce(lambda tx_sum, tx_amt: tx_sum + sum(tx_amt)
                             if len(tx_amt) > 0 else tx_sum + 0, tx_recipient, 0)
    # Retorna el balance total de una cuenta en concreto
    return amount_received - amount_sent


def get_last_blockchain_value():
    
    """ Retorna el ultimo valor del blockchain actual. """
    if len(blockchain) < 1:
        return None
    return blockchain[-1]


def verify_transaction(transaction):
    """Verifica que la persona que envia la transaccion tiene la suficiente cantidad.

    Arguments:
        :transaction: La transaccion que sera verificada.
    """
    sender_balance = get_balance(transaction['sender'])
    return sender_balance >= transaction['amount']

def add_transaction(recipient, sender=owner, amount=1.0):
    
    """ Mete un nuevo valor al final del blockchain con toda la info de la transaccion.

    Arguments:
        :sender: El emisor de la transaccion.
        :recipient: El receptor de la transaccion.
        :amount: La cantidad de monedas enviadas en la transaccion (default = 1.0)
    """
    transaction = {
        'sender': sender,
        'recipient': recipient,
        'amount': amount
    }
    if verify_transaction(transaction):
        open_transactions.append(transaction)
        participants.add(sender)
        participants.add(recipient)
        return True
    return False


def mine_block():
    
    """Creamos un nuevo bloque y anade nuevas transacciones.."""
    # Lista el ultimo bloque del blockchain
    last_block = blockchain[-1]
    # Hashea el bloque
    hashed_block = hash_block(last_block)
    print(hashed_block)
    # Miner reward
    reward_transaction = {
        'sender': 'MINING',
        'recipient': owner,
        'amount': MINING_REWARD
    }
    # Copia la transaccion en lugar de manipular la lista original de open_transactions
    copied_transactions = open_transactions[:]
    copied_transactions.append(reward_transaction)
    block = {
        'previous_hash': hashed_block,
        'index': len(blockchain),
        'transactions': copied_transactions
    }
    blockchain.append(block)
    return True


def get_transaction_value():
    """ Retorna la entrada por teclado que mete el usuario. """
   
    tx_recipient = input('Escribe por favor el receptor de la transaccion: ')
    tx_amount = float(input('Cantidad a transferir: '))
    return tx_recipient, tx_amount


def get_user_choice():
    """Solicita al usuario su eleccion y la devuelve.."""
    user_input = input('Elige una opcion: ')
    return user_input


def print_blockchain_elements():
    """ Salida de todos los bloques del blockchain. """
    for block in blockchain:
        print('Outputting Block')
        print(block)
    else:
        print('-' * 20)


def verify_chain():
    """ Verifique la cadena de bloques actual y devuelva Verdadero si es valido,y en caso contrario devuelve falso."""
    for (index, block) in enumerate(blockchain):
        if index == 0:
            continue
        if block['previous_hash'] != hash_block(blockchain[index - 1]):
            return False
    return True


def verify_transactions():
    """Verificas todas las transacciones abiertas."""
    return all([verify_transaction(tx) for tx in open_transactions])


waiting_for_input = True

# Pinto un while loop para 'La interfaz de usuario'
while waiting_for_input:
    print('Por favor elige una opcion:')
    print('1: Hacer una nueva transaccion')
    print('2: Minar un nuevo bloque')
    print('3: Pinta los bloques de la sesion')
    print('4: Ver participantes')
    print('5: Chequear la validacion de la transacion')
    print('h: Manipular el chain')
    print('q: Salir')
    user_choice = get_user_choice()
    if user_choice == 1:
        tx_data = get_transaction_value()
        recipient, amount = tx_data
        # Anade el valor de la transacion al blockchain
        if add_transaction(recipient, amount=amount):
            print('Transaccion hecha correctamente!')
        else:
            print('Transaccion falliad!')
        print(open_transactions)
    elif user_choice == 2:
        if mine_block():
            open_transactions = []
    elif user_choice == 3:
        print_blockchain_elements()
    elif user_choice == 4:
        print(participants)
    elif user_choice == 5:
        if verify_transactions():
            print('Todas las transacciones son validas')
        else:
            print('Transaciones invalida')
    elif user_choice == 'h':
        # Me aseguro de que no se puede 'hackear' el blockchain si esta vacio xDD
        if len(blockchain) >= 1:
            blockchain[0] = {
                'previous_hash': '',
                'index': 0,
                'transactions': [{'sender': 'Chris', 'recipient': 'Flavius', 'amount': 100.0}]
            }
    elif user_choice == 'q':
        
        waiting_for_input = False
    else:
        print('Has elegido una mala opcion.Por favor vuelve a elegir!')
    if not verify_chain():
        print_blockchain_elements()
        print('Invalid blockchain!')
        # Break out of the loop
        break
    print('El saldo de la cuenta de {} es de:{:6.2f}'.format('Flavius', get_balance('Flavius')))
else:
    print('Saliendo de la sesion')


print('Listo!')